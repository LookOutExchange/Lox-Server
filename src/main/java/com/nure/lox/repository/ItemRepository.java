package com.nure.lox.repository;

import com.nure.lox.repository.entity.Category;
import com.nure.lox.repository.entity.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository  extends PagingAndSortingRepository<Item, Long> {

    Page<Item> findByCategoryAndStatus(Category category, Boolean status, Pageable pageable);

    Page<Item> findByCategoryAndTitleAndStatus(Category category, String title, Boolean status, Pageable pageable);

    Page<Item> findByStatus(Boolean status, Pageable pageable);

    Page<Item> findByStatusAndTitle(Boolean status, String Title, Pageable pageable);
}
