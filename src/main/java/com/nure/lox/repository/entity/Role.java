package com.nure.lox.repository.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

import static javax.persistence.GenerationType.*;

@Entity
@Data
@NoArgsConstructor
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    private String title;

    @Builder
    public Role(Integer id, String title) {
     //   this.id = id;
        this.title = title;
    }
}
