package com.nure.lox.repository.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class Feature implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String value;

    @ManyToMany(mappedBy = "features")
    private Set<Item> items = new HashSet<>();
}
