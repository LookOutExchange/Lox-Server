package com.nure.lox.repository.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String firstName;

    private String secondName;

    private String email;

    private String password;

    private Boolean status;

    @ManyToOne
    private Role role;

    @Builder
    public User(Long id, String firstName, String secondName, String email, String password, Boolean status, Role role) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
        this.status = status;
        this.role = role;
    }
}
