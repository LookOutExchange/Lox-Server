package com.nure.lox.repository.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Category implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String value;

    @Builder
    public Category(Long id, String title, String value) {
        this.id = id;
        this.title = title;
        this.value = value;
    }
}
