package com.nure.lox.repository.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class Item implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private Integer price;

    private String address;

    private Boolean status;

    private LocalDateTime createdDate;

    private String photo;

    private String description;

    private Long views;

    private String phone;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany
    @JoinTable(
            name = "Item_Feature",
            joinColumns = {@JoinColumn(name = "item_id")},
            inverseJoinColumns = {@JoinColumn(name = "feature_id")}
    )
    private Set<Feature> features = new HashSet<>();

    @Builder
    public Item(Long id, String title, Integer price, String address, Boolean status,
                LocalDateTime createdDate, String photo, String description, Category category,
                User user, Set<Feature> features, Long views, String phone) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.address = address;
        this.status = status;
        this.createdDate = createdDate;
        this.photo = photo;
        this.description = description;
        this.category = category;
        this.user = user;
        this.features = features;
        this.views = views;
        this.phone = phone;
    }

    public void addView(){
        views++;
    }
}
