package com.nure.lox.repository;

import com.nure.lox.repository.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    @Override
    List<User> findAll();

    User findByEmail(String email);

    @Query("UPDATE User set status = :status where email = :email")
    void updateStatusByEmail(Boolean status, String email);

    @Query("UPDATE User set firstName = :firstName where id=:id")
    void updateUserDetails(User user);
}
