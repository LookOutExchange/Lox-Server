package com.nure.lox.repository;

import com.nure.lox.repository.entity.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    Category findByTitle(String title);

    Category findByValue(String value);

    @Override
    List<Category> findAll();
}
