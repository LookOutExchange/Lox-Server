package com.nure.lox.controller.impl;

import com.nure.lox.controller.IItemController;
import com.nure.lox.controller.dto.CreateItemDto;
import com.nure.lox.controller.dto.ItemDto;
import com.nure.lox.controller.dto.ItemPageDto;
import com.nure.lox.repository.entity.Category;
import com.nure.lox.repository.entity.Item;
import com.nure.lox.service.ICategoryService;
import com.nure.lox.service.IItemService;
import com.nure.lox.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.Objects.nonNull;
import static org.springframework.web.context.request.RequestAttributes.SCOPE_SESSION;
import static org.springframework.web.context.request.RequestContextHolder.getRequestAttributes;

@RestController
@RequestMapping("/item")
public class ItemController implements IItemController {

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private IItemService itemService;

    @Autowired
    private IUserService userService;

    @Override
    @GetMapping("/retrieve/{page}")
    public ItemPageDto retrieveItems(
            @PathVariable Integer page,
            @RequestParam(required = false) String categoryValue,
            @RequestParam(required = false, defaultValue = DEFAULT_ORDER_BY) String orderBy,
            @RequestParam(required = false, defaultValue = DEFAULT_ORDER_DIRECTION) String orderDirection,
            @RequestParam(required = false) String title) {
        List<ItemDto> itemsForPage = itemService
                .getItemsForPage(categoryValue, page, DEFAULT_ITEMS_ON_PAGE, orderDirection, orderBy, title);
        return ItemPageDto
                .builder()
                .items(itemsForPage)
                .pages(itemsForPage.size() / DEFAULT_ITEMS_ON_PAGE)
                .build();
    }

    @Override
    @GetMapping("/retrieve/newest")
    public List<ItemDto> retrieveNewest() {
        return itemService.getNewest();
    }

    @Override
    @GetMapping("/retrieveOne/{itemId}")
    public Item retrieveItem(@PathVariable Long itemId) {
        return itemService.getItem(itemId, (Long) getRequestAttributes().getAttribute("userId", SCOPE_SESSION));
    }

    @Override
    @GetMapping("/category/retrieve")
    public List<Category> retrieveCategories() {
        return categoryService.retrieveCategories();
    }

    @Override
    @PostMapping("/category/create")
    public void createCategory(@RequestParam String title, @RequestParam String value) {
        categoryService.createNewCategory(title, value);
    }

    @Override
    @DeleteMapping("/delete/{itemId}")
    public void deleteItem(@PathVariable Long itemId) {
        itemService.dropItem(itemId);
    }

    @Override
    @PostMapping("/status/{itemId}")
    public void changeItemStatus(@PathVariable Long itemId, @RequestParam Boolean status) {
        itemService.changeItemStatus(itemId, status);
    }

    @Override
    @PostMapping("/create")
    public boolean createItem(@RequestBody CreateItemDto item) {
        Long userId = (Long) getRequestAttributes().getAttribute("userId", SCOPE_SESSION);
        if (nonNull(userId)) {
            item.setUser(userService.retrieveUser(userId));
            return itemService.createNewItem(item);
        }
        return false;
    }

    @Override
    @PostMapping("/edit")
    public void editItem(Item item) {

    }
}