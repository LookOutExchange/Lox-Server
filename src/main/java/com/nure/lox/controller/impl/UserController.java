package com.nure.lox.controller.impl;

import com.nure.lox.controller.IUserController;
import com.nure.lox.controller.dto.UserDto;
import com.nure.lox.controller.dto.UserLoginDto;
import com.nure.lox.repository.entity.User;
import com.nure.lox.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_SESSION;
import static org.springframework.web.context.request.RequestContextHolder.getRequestAttributes;

@RestController
@RequestMapping("/user")
public class UserController implements IUserController {

    @Autowired
    private IUserService userService;

    @Override
    @GetMapping(path = "/retrieve")
    public List<User> retrieveAllUsers() {
        return userService.retrieveAll();
    }

    @Override
    public User retrieveUser(String email) {
        return userService.retrieveUser(email);
    }

    @Override
    @PostMapping(path = "/block")
    public void blockUser(String email) {
        userService.blockUser(email);
    }

    @Override
    @PutMapping("/login")
    public UserLoginDto login(
            @RequestParam String email,
            @RequestParam String password) {
        if (email == null || password == null) {
            return null;
        }
        UserLoginDto userLoginDto = userService.login(email, password);
        getRequestAttributes().setAttribute("userId", userLoginDto.getUserId(), SCOPE_SESSION);
        return userLoginDto;
    }

    @Override
    @GetMapping("/logout")
    public void logout() {
        getRequestAttributes().removeAttribute("userId", SCOPE_SESSION);
    }

    @Override
    @PostMapping("/signUp")
    public boolean singUp(@RequestBody UserDto userDto) {
        return userService.createNewUser(userDto);
    }
}
