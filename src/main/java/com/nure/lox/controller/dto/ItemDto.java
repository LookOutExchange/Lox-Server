package com.nure.lox.controller.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ItemDto {

    private Long id;

    private String title;

    private Integer price;

    private String photo;

    @Builder
    public ItemDto(Long id, String title, Integer price, String photo) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.photo = photo;
    }
}
