package com.nure.lox.controller.dto;

import com.nure.lox.repository.entity.Category;
import com.nure.lox.repository.entity.Feature;
import com.nure.lox.repository.entity.User;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateItemDto {

    private String title;

    private Integer price;

    private String address;

    private String photo;

    private String description;

    private Category category;

    private User user;

    private Feature[] features;

    private String phone;

    @Builder
    public CreateItemDto(String title, Integer price, String address, String photo, String description, Category category, User user, Feature[] features, String phone) {
        this.title = title;
        this.price = price;
        this.address = address;
        this.photo = photo;
        this.description = description;
        this.category = category;
        this.user = user;
        this.features = features;
        this.phone = phone;
    }
}
