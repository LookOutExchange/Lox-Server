package com.nure.lox.controller.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class UserLoginDto {

    private Long userId;

    private String firstName;

    private String secondName;

    private String email;

    @Builder
    public UserLoginDto(Long userId, String firstName, String secondName, String email) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.userId = userId;
    }
}
