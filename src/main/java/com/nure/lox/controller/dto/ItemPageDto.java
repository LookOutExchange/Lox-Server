package com.nure.lox.controller.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
public class ItemPageDto {

    private List<ItemDto> items;

    private int pages;

    @Builder
    public ItemPageDto(List<ItemDto> items, double pages) {
        this.items = items;
        this.pages = (pages > (int) pages) ? (int) pages + 1 : (int) pages;
    }
}
