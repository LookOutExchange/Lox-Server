package com.nure.lox.controller;

import com.nure.lox.controller.dto.UserDto;
import com.nure.lox.controller.dto.UserLoginDto;
import com.nure.lox.repository.entity.User;

import java.util.List;

public interface IUserController {

    List<User> retrieveAllUsers();

    User retrieveUser(String email);

    void blockUser(String email);

    UserLoginDto login(String email, String password);

    void logout();

    boolean singUp(UserDto userDto);
}
