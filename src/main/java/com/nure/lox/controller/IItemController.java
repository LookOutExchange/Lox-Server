package com.nure.lox.controller;

import com.nure.lox.controller.dto.CreateItemDto;
import com.nure.lox.controller.dto.ItemDto;
import com.nure.lox.controller.dto.ItemPageDto;
import com.nure.lox.repository.entity.Category;
import com.nure.lox.repository.entity.Item;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface IItemController {

    int DEFAULT_ITEMS_ON_PAGE = 10;
    String DEFAULT_ORDER_BY = "createdDate";
    String DEFAULT_ORDER_DIRECTION = "desc";

    /**
     * Returns page of active items in specified category with
     * specified title, ordered by orderBy parameter in orderDirection.
     *
     * @param page           page number, not null
     * @param categoryValue       category english value
     * @param orderBy        attribute to order by
     * @param orderDirection order direction(asc/desc)
     * @return list of items
     */
    ItemPageDto retrieveItems(Integer page, String categoryValue, String orderBy, String orderDirection, String title);

    List<ItemDto> retrieveNewest();

    Item retrieveItem(Long id);

    List<Category> retrieveCategories();

    /**
     * @param title the title of new category, not null
     * @param value english value of new category, not null
     */
    void createCategory(String title, String value);

    /**
     * @param itemId id of item to be deleted
     */
    void deleteItem(Long itemId);

    /**
     * @param itemId id of item to be changed
     * @param status new status
     */
    void changeItemStatus(Long itemId, Boolean status);

    boolean createItem(@RequestBody CreateItemDto item);

    void editItem(Item item);
}
