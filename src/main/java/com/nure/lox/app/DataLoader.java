package com.nure.lox.app;

import com.nure.lox.repository.*;
import com.nure.lox.repository.entity.Category;
import com.nure.lox.repository.entity.Item;
import com.nure.lox.repository.entity.Role;
import com.nure.lox.repository.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private RoleRepository roleRepository;

    public void run(ApplicationArguments args) {
        addCategories();
        Role role = addRoles();
        List<User> userList = addUsers(role);
        addItems(userList);
    }

    private void addItems(List<User> userList) {
        for (int i = 0; i < 10; i++) {
            itemRepository.save(Item.builder()
                    .status(true)
                    .title("title" + i)
                    .price(i)
                    .photo("photo" + i)
                    .address("address" + i)
                    .category(categoryRepository.findAll().iterator().next())
                    .description("des" + i)
                    .user(userList.get(i))
                    .views((long) i)
                    .build());
        }
    }

    private Role addRoles() {
        return roleRepository.save(Role.builder().title("USER").build());
    }

    private List<User> addUsers(Role role) {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            users.add(userRepository.save(User
                    .builder()
                    .email("email" + i)
                    .firstName("name" + i)
                    .secondName("secondName" + i)
                    .password("pass")
                    .role(role)
                    .status(true)
                    .build()));
        }
        return users;
    }

    private void addCategories() {
        for (int i = 0; i < 10; i++) {
            categoryRepository.save(Category
                    .builder()
                    .title("названиеКатегории" + i)
                    .value("categoryValue" + i)
                    .build());
        }
    }
}
