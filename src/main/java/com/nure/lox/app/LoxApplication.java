package com.nure.lox.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@EntityScan("com.nure.lox.repository.entity")
@EnableJpaRepositories("com.nure.lox.repository")
@ComponentScan({
        "com.nure.lox.controller",
        "com.nure.lox.service",
        "com.nure.lox.repository",
        "com.nure.lox.repository.entity",
        "com.nure.lox.app"})
public class LoxApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoxApplication.class, args);
    }
}
