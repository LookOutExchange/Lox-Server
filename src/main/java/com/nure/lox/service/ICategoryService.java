package com.nure.lox.service;

import com.nure.lox.repository.entity.Category;

import java.util.List;

public interface ICategoryService {

    List<Category> retrieveCategories();

    void createNewCategory(String title, String value);
}
