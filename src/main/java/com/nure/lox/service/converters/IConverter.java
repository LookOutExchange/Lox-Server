package com.nure.lox.service.converters;

public interface IConverter<S, T> {

    T convert(S source);
}
