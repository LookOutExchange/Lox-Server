package com.nure.lox.service.converters;

import com.nure.lox.controller.dto.ItemDto;
import com.nure.lox.repository.entity.Item;
import org.springframework.stereotype.Component;

@Component
public class ItemToItemDtoConverter implements IConverter<Item, ItemDto> {

    @Override
    public ItemDto convert(Item source) {
        return ItemDto.builder()
                .id(source.getId())
                .photo(source.getPhoto())
                .title(source.getTitle())
                .price(source.getPrice())
                .build();
    }
}
