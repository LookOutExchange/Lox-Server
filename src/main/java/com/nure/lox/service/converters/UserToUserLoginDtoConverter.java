package com.nure.lox.service.converters;

import com.nure.lox.controller.dto.UserLoginDto;
import com.nure.lox.repository.entity.User;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class UserToUserLoginDtoConverter implements IConverter<User, UserLoginDto> {

    @Override
    public UserLoginDto convert(User source) {
        if (isNull(source)) {
            return null;
        }
        return UserLoginDto
                .builder()
                .email(source.getEmail())
                .firstName(source.getFirstName())
                .secondName(source.getSecondName())
                .userId(source.getId())
                .build();
    }
}
