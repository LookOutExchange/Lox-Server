package com.nure.lox.service.converters;

import com.nure.lox.controller.dto.CreateItemDto;
import com.nure.lox.repository.entity.Feature;
import com.nure.lox.repository.entity.Item;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class CreateItemDtoToItemConverter implements IConverter<CreateItemDto, Item> {

    @Override
    public Item convert(CreateItemDto source) {
        return Item.builder()
                .address(source.getAddress())
                .category(source.getCategory())
                .createdDate(LocalDateTime.now())
                .description(source.getDescription())
                .features(getFeatures(source))
                .price(source.getPrice())
                .status(true)
                .photo(source.getPhoto())
                .user(source.getUser())
                .title(source.getTitle())
                .phone(source.getPhone())
                .build();
    }

    private Set<Feature> getFeatures(CreateItemDto source) {
        Set featuresSet = new HashSet();
        featuresSet.addAll(Arrays.asList(source.getFeatures()));
        return featuresSet;
    }
}
