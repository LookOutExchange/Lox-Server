package com.nure.lox.service.impl;

import com.nure.lox.controller.dto.CreateItemDto;
import com.nure.lox.controller.dto.ItemDto;
import com.nure.lox.repository.CategoryRepository;
import com.nure.lox.repository.ItemRepository;
import com.nure.lox.repository.UserRepository;
import com.nure.lox.repository.entity.Category;
import com.nure.lox.repository.entity.Item;
import com.nure.lox.service.IItemService;
import com.nure.lox.service.converters.CreateItemDtoToItemConverter;
import com.nure.lox.service.converters.ItemToItemDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.StringUtils.isEmpty;

@Service
public class ItemService implements IItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ItemToItemDtoConverter itemToItemDto;

    @Autowired
    private CreateItemDtoToItemConverter createItemDtoToItem;

    @Override
    public void dropItem(Long id) {
        itemRepository.delete(new Long(id));
    }

    @Override
    public void changeItemStatus(Long itemId, Boolean status) {
        Item item = itemRepository.findOne(new Long(itemId));
        item.setStatus(status);
        itemRepository.save(item);
    }

    @Override
    public Set<Item> getAllUserItems(Boolean status) {
        return null;
    }

    @Override
    public Set<Item> getAllItems(Boolean status) {
        return null;
    }

    @Override
    public List<ItemDto> getItemsForPage(@Nullable String categoryValue, int page, int itemsOnPage,
                                         @Nullable String orderDirection, @Nullable String orderBy, @Nullable String title) {

        Page<Item> result;

        Sort.Direction direction = Sort.Direction.fromString(orderDirection);
        Pageable pageable = new PageRequest(page, itemsOnPage, direction, orderBy);

        if (isEmpty(categoryValue)) {
            if (isEmpty(title)) {
                result = itemRepository.findByStatus(true, pageable);
            } else {
                result = itemRepository.findByStatusAndTitle(true, title, pageable);
            }
        } else {
            Category category = categoryRepository.findByValue(categoryValue);
            if (isEmpty(title)) {
                result = itemRepository.findByCategoryAndStatus(category, true, pageable);
            } else {
                result = itemRepository.findByCategoryAndTitleAndStatus(category, title, true, pageable);
            }
        }
        return result
                .getContent()
                .stream()
                .map(itemToItemDto::convert)
                .collect(toList());
    }

    @Override
    public boolean createNewItem(CreateItemDto item) {
        return nonNull(itemRepository.save(createItemDtoToItem.convert(item)));
    }

    @Override
    public int getPagesAmount(int itemsOnPage) {
        return 0;
    }

    @Override
    public Item getItem(Long id, Long userId) {
        Item item = itemRepository.findOne(id);
        saveViews(userId, item);
        return item;
    }

    @Override
    public List<ItemDto> getNewest() {
        return itemRepository.findAll(new PageRequest(0, 5, Sort.Direction.DESC, "createdDate"))
                .getContent()
                .stream()
                .map(itemToItemDto::convert)
                .collect(toList());
    }

    private void saveViews(Long userId, Item item) {
        if (nonNull(userId) && !(userId).equals(item.getUser().getId())) {
            item.addView();
            itemRepository.save(item);
        }
    }
}
