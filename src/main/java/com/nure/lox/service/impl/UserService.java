package com.nure.lox.service.impl;

import com.nure.lox.controller.dto.UserDto;
import com.nure.lox.controller.dto.UserLoginDto;
import com.nure.lox.repository.UserRepository;
import com.nure.lox.repository.entity.User;
import com.nure.lox.service.IUserService;
import com.nure.lox.service.converters.UserToUserLoginDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserToUserLoginDtoConverter userToUserLoginDtoConverter;

    @Override
    public UserLoginDto login(String email, String password) {
        User user = userRepository.findByEmail(email);
        if (user != null && user.getPassword().equals(password) && user.getStatus()) {
            return userToUserLoginDtoConverter.convert(user);
        }
        return null;
    }

    @Override
    public boolean signUp(User user) {
        return false;
    }

    @Override
    public List<User> retrieveAll() {
        return userRepository.findAll();
    }

    @Override
    public User retrieveUser(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User retrieveUser(Long userId) {
        return userRepository.findOne(userId);
    }

    @Override
    public void blockUser(String email) {
        userRepository.updateStatusByEmail(false, email);
    }

    @Override
    public boolean changeUserDetails(String firstName, String secondName, String password, String email, String currentEmail) {
        Long id = userRepository.findByEmail(currentEmail).getId();
        userRepository.updateUserDetails(User.builder()
                .email(email)
                .id(id)
                .firstName(firstName)
                .password(password)
                .secondName(secondName)
                .build());
        return true;
    }

    @Override
    public boolean createNewUser(UserDto userDto) {
        if (!validate(userDto)) {
            return false;
        }
        User user = userRepository.save(User.builder()
                .secondName(userDto.getSecondName())
                .firstName(userDto.getFirstName())
                .password(userDto.getPassword())
                .email(userDto.getEmail())
                .status(true)
                .build());
        return user != null;
    }

    private boolean validate(UserDto userDto) {
        return (isNull(userDto) || isNull(userDto.getEmail()) || isNull(userDto.getFirstName()) ||
                isNull(userDto.getPassword()) || isNull(userDto.getSecondName()));
    }
}
