package com.nure.lox.service.impl;

import com.nure.lox.repository.CategoryRepository;
import com.nure.lox.repository.entity.Category;
import com.nure.lox.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService implements ICategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> retrieveCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public void createNewCategory(String title, String value) {
        categoryRepository.save(Category.builder()
                .title(title)
                .value(value)
                .build());
    }
}
