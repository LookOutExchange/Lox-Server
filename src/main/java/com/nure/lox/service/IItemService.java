package com.nure.lox.service;

import com.nure.lox.controller.dto.CreateItemDto;
import com.nure.lox.controller.dto.ItemDto;
import com.nure.lox.repository.entity.Item;

import java.util.List;
import java.util.Set;

public interface IItemService {

    void dropItem(Long id);

    void changeItemStatus(Long itemId, Boolean status);

    Set<Item> getAllUserItems(Boolean status);

    Set<Item> getAllItems(Boolean status);

    List<ItemDto> getItemsForPage(String categoryName, int page, int itemsOnPage, String orderDirection, String orderBy, String title);

    boolean createNewItem(CreateItemDto item);

    int getPagesAmount(int itemsOnPage);

    Item getItem(Long id, Long userId);

    List<ItemDto> getNewest();
}
