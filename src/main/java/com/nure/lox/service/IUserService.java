package com.nure.lox.service;

import com.nure.lox.controller.dto.UserDto;
import com.nure.lox.controller.dto.UserLoginDto;
import com.nure.lox.repository.entity.User;

import java.util.List;

public interface IUserService {

    UserLoginDto login(String email, String password);

    boolean signUp(User user);

    List<User> retrieveAll();

    User retrieveUser(String email);

    User retrieveUser(Long userId);

    void blockUser(String email);

    boolean changeUserDetails(String firstName, String secondName, String password, String email, String currentEmail);

    boolean createNewUser(UserDto userDto);
}
