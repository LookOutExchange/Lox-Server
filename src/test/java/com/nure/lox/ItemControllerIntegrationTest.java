package com.nure.lox;

import com.nure.lox.repository.entity.Category;
import com.nure.lox.service.IItemService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class ItemControllerIntegrationTest extends CommonIntegrationTest{

    @Autowired
    private IItemService iItemService;

    @Test
    public void getCategoryTest() throws Exception {
        List<Category> categories = iItemController.retrieveCategories();
        assertNotNull(categories);
        assertEquals(categories.size(), 10);
    }

}
