package com.nure.lox;

import com.nure.lox.app.LoxApplication;
import com.nure.lox.controller.IItemController;
import com.nure.lox.repository.CategoryRepository;
import com.nure.lox.repository.ItemRepository;
import com.nure.lox.repository.RoleRepository;
import com.nure.lox.repository.UserRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LoxApplication.class)
@WebAppConfiguration
public class CommonIntegrationTest {

    @Autowired
    protected IItemController iItemController;

    @Autowired
    protected CategoryRepository categoryRepository;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected RoleRepository roleRepository;

    @Autowired
    protected ItemRepository itemRepository;

//    @Before
//    public void init() {
//        addCategories();
//        Role role = addRoles();
//        List<User> userList = addUsers(role);
//        addItems(userList);
//    }
//
//    private void addItems(List<User> userList) {
//        for (int i = 0; i < 10; i++) {
//            itemRepository.save(Item.builder()
//                    .status(true)
//                    .title("title" + i)
//                    .price(i)
//                    .photo("photo" + i)
//                    .address("address" + i)
//                    .category(categoryRepository.findAll().iterator().next())
//                    .description("des" + i)
//                    .user(userList.get(i))
//                    .build());
//        }
//    }
//
//    private Role addRoles() {
//        return roleRepository.save(Role.builder().title("USER").build());
//    }
//
//    private List<User> addUsers(Role role) {
//        List<User> users = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            users.add(userRepository.save(User
//                    .builder()
//                    .email("email" + i)
//                    .firstName("name" + i)
//                    .secondName("secondName" + i)
//                    .password("pass")
//                    .role(role)
//                    .status(true)
//                    .build()));
//        }
//        return users;
//    }
//
//    private void addCategories() {
//        for (int i = 0; i < 10; i++) {
//            categoryRepository.save(Category
//                    .builder()
//                    .title("названиеКатегории" + i)
//                    .value("categoryValue" + i)
//                    .build());
//        }
//    }
//
//    @After
//    public void clean() {
//        itemRepository.deleteAll();
//        userRepository.deleteAll();
//        roleRepository.deleteAll();
//        categoryRepository.deleteAll();
//    }
}
