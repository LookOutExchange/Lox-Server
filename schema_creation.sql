-- MySQL Script generated by MySQL Workbench
-- 10/25/17 14:24:48
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema lox
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `lox` ;

-- -----------------------------------------------------
-- Schema lox
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `lox` DEFAULT CHARACTER SET utf8 ;
USE `lox` ;

-- -----------------------------------------------------
-- Table `lox`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lox`.`category` ;

CREATE TABLE IF NOT EXISTS `lox`.`category` (
  `id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lox`.`item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lox`.`item` ;

CREATE TABLE IF NOT EXISTS `lox`.`item` (
  `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(245) NOT NULL,
  `price` FLOAT UNSIGNED NOT NULL,
  `address` VARCHAR(140) NOT NULL,
  `status` SMALLINT(1) NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `photo` CHAR(15) NULL,
  `description` TEXT(1024) NULL,
  `category_id` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `category_id`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC),
  UNIQUE INDEX `photo_UNIQUE` (`photo` ASC),
  INDEX `fk_item_category1_idx` (`category_id` ASC),
  CONSTRAINT `fk_item_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `lox`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lox`.`feature`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lox`.`feature` ;

CREATE TABLE IF NOT EXISTS `lox`.`feature` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) NOT NULL,
  `value` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lox`.`feature_has_item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lox`.`feature_has_item` ;

CREATE TABLE IF NOT EXISTS `lox`.`feature_has_item` (
  `feature_id` SMALLINT UNSIGNED NOT NULL,
  `item_id` MEDIUMINT UNSIGNED NOT NULL,
  PRIMARY KEY (`feature_id`, `item_id`),
  INDEX `fk_feature_has_item_item1_idx` (`item_id` ASC),
  INDEX `fk_feature_has_item_feature1_idx` (`feature_id` ASC),
  CONSTRAINT `fk_feature_has_item_feature1`
    FOREIGN KEY (`feature_id`)
    REFERENCES `lox`.`feature` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_feature_has_item_item1`
    FOREIGN KEY (`item_id`)
    REFERENCES `lox`.`item` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
